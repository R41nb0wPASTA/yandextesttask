using UnityEngine;

[CreateAssetMenu(fileName = "ItemData", menuName = "ItemData", order = 1100)]
public class ItemData : ScriptableObject
{
    [Header("Item")]
    public float itemSpeed = 8;

    public float itemXSpawn = 1.06f;
    public float itemMinYSpawnInclusive = 0.2f;
    public float itemMaxYSpawnExclusive = 0.9f;
    public float itemZSpawn = 10;

    [Header("PointSquareItem")]
    public float pointSquareGravitySpeed = 6;
    public float pointSquareGravityDistance = 3;
    
    public float pointSquareMinSpawnTimeStep = 3;
    public float pointSquareMaxSpawnTimeStep = 6;

    [Header("KillerSquareItem")]
    public float killerSquareVerticalSpeedSmoothing = 0.8f;
    public float killerSquareVerticalStepHalf = 1.5f;
    
    public float killerSquareMinSpawnTimeStep = 0.2f;
    public float killerSquareMaxSpawnTimeStep = 3;
}
