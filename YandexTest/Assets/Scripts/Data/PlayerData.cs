using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "PlayerData", order = 1100)]
public class PlayerData : ScriptableObject
{
    [Header("Player")]
    public float playerSpeed = 12;

    [Header("Player Trail")]
    public float playerTrailTimeStep = 0.1f;
    public Vector3 playerTrailScaleOverTime = new Vector3(0.25f, 0.25f, 0.25f);
    public float playerTrailAlphaScaleOverTime = 0.025f;
}

