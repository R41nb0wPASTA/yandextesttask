using UnityEngine;

public class ItemLogic : MonoBehaviour
{
    [Header("Data")]
    public ItemData id;

    private void FixedUpdate()
    {
        //Method for overriding
        CustFixedUpdate();
        
        //Move item left
        transform.Translate(Vector3.left * id.itemSpeed * Time.deltaTime);
    }

    private void Update()
    {
        //Destroy item when out of camera bounds
        if (Camera.main.WorldToViewportPoint(transform.position).x < -0.2f)
            Destroy(gameObject);
    }

    protected virtual void CustFixedUpdate() {}
}
