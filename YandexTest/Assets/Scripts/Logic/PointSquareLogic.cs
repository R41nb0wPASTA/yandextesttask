using UnityEngine;

public class PointSquareLogic : ItemLogic
{
    private Transform player;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    protected override void CustFixedUpdate()
    {
        //Move towards player if in close range
        if (Vector3.Distance(transform.position, player.position) < id.pointSquareGravityDistance)
        {
            float step =  id.pointSquareGravitySpeed * Time.fixedDeltaTime;
            transform.position = Vector3.MoveTowards(transform.position, player.position, step);
        }
    }
}
