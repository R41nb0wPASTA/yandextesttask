using System;
using UnityEngine;
using UnityEngine.UI;

public class GUILogic : MonoBehaviour
{
    [Header("UI")]
    public Text scoreCounterText;

    public Text pressToStartText;

    public void SetScoreText(String score)
    {
        scoreCounterText.text = score;
    }

    public void HidePTSText()
    {
        pressToStartText.enabled = false;
    }

    public void ShowPTSText()
    {
        pressToStartText.enabled = true;
    }
}
