using UnityEngine;

public class PlayerTrailLogic : ItemLogic
{
    [Header("Data")]
    public PlayerData pd;
    
    protected override void CustFixedUpdate()
    {
        //Downscale trail part over time
        transform.localScale -= pd.playerTrailScaleOverTime * Time.fixedDeltaTime;

        //Downscale alpha color of trail part over time
        Color aColor = GetComponent<SpriteRenderer>().color;
        aColor.a -= pd.playerTrailAlphaScaleOverTime;
        GetComponent<SpriteRenderer>().color = aColor;
    }
}
