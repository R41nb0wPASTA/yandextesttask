using UnityEngine;
using Random = UnityEngine.Random;

public class KillerSquareLogic : MonoBehaviour
{
    [Header("Data")]
    public ItemData id;
    
    private Vector3 topMarker;
    private Vector3 botMarker;
    private Vector3 curMarker;

    private void Awake()
    {
        //Set top and bot borders for vertical movement
        topMarker = Vector3.up * id.killerSquareVerticalStepHalf;
        botMarker = Vector3.down * id.killerSquareVerticalStepHalf;
        
        if (Random.Range(0, 2) == 1)
            curMarker = topMarker;
        else
            curMarker = botMarker;
    }

    private void FixedUpdate()
    {
        //Move up and down
        transform.localPosition = Vector3.Lerp (transform.localPosition, curMarker, id.killerSquareVerticalSpeedSmoothing * Time.fixedDeltaTime);
        if (Vector3.Distance(transform.localPosition, curMarker) < 0.3f)
        {
            if (curMarker == topMarker)
                curMarker = botMarker;
            else
                curMarker = topMarker;
        }
    }
}
