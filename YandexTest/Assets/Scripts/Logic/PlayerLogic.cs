using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class PlayerEncounteredItemEvent : UnityEvent<Collider2D> {}

public class PlayerLogic : MonoBehaviour
{
    [Header("Data")]
    public PlayerData pd;
    
    [Header("Prefab")]
    public GameObject playerTrailPrefab;
    
    public PlayerEncounteredItemEvent PlayerEncounteredItemEvent;

    private Vector2 _playerGravity;
    
    private void Awake()
    {
        if (pd.playerSpeed > 0)
            pd.playerSpeed *= -1;

        _playerGravity = new Vector2(0f, pd.playerSpeed);
    }

    private void Start()
    {
        StartCoroutine(SpawnPlayerTrail());
    }
    
    void FixedUpdate()
    {
        //Move Player
        GetComponent<Rigidbody2D>().AddForce(_playerGravity);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //Inform Game Logic about encounter
        PlayerEncounteredItemEvent.Invoke(col);
    }
    
    public void ReversePlayerMovementDir()
    {
        _playerGravity *= -1;
    }
    
    private IEnumerator SpawnPlayerTrail()
    {
        while (true)
        {
            //Spawn trail part every time step
            yield return new WaitForSeconds(pd.playerTrailTimeStep * Time.fixedDeltaTime);
            
            //Moving trail parts further back in order to not overlap point squares
            Vector3 playerTransformBGPos = transform.position;
            playerTransformBGPos = new Vector3(playerTransformBGPos.x, playerTransformBGPos.y, playerTransformBGPos.z + 1f);
            Instantiate(playerTrailPrefab, playerTransformBGPos, transform.rotation);
        }
    }
}
