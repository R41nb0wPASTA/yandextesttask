using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour
{
    [Header("Data")]
    public ItemData id;
    
    [Header("Logic")]
    public PlayerLogic player;
    public GUILogic guiLogic;

    [Header("Prefab")]
    public GameObject killerSquarePrefab;
    public GameObject pointSquarePrefab;
    
    private bool _buttonPressed = false;
    private bool _isFirstPress = true;

    private int _score = 0;

    private void OnEnable()
    {
        //Add listener to player encounter with item
        player.PlayerEncounteredItemEvent.AddListener(OnPlayerEncounteredItem);
    }
    
    private void OnDisable()
    {
        player.PlayerEncounteredItemEvent.RemoveListener(OnPlayerEncounteredItem);
    }

    private void Awake()
    {
        //Pause game and reset score
        guiLogic.SetScoreText("0");
        Time.timeScale = 0;
    }

    private void Update()
    {
        CheckInputs();
    }

    private void StartGame()
    {
        guiLogic.HidePTSText();
        Time.timeScale = 1;
        
        StartCoroutine(SpawnKillerSquare());
        StartCoroutine(SpawnPointSquare());
    }
    
    private void CheckInputs()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            //Unpause game
            if (_isFirstPress)
            {
                _isFirstPress = false;
                StartGame();
            }

            //If key pressed set player gravity to top
            if (_buttonPressed) return;
            _buttonPressed = !_buttonPressed;
            player.ReversePlayerMovementDir();
        }
        else
        {
            //If key unpressed set player gravity to bot
            if (!_buttonPressed) return;
            _buttonPressed = !_buttonPressed;
            player.ReversePlayerMovementDir();
        }
    }

    private void OnPlayerEncounteredItem(Collider2D encCol)
    {
        //Increase score on point square encounter
        if (encCol.gameObject.CompareTag("PointSquare"))
        {
            Destroy(encCol.gameObject);
            _score++;
            guiLogic.SetScoreText(_score.ToString());
        }
        //Reset game on killer square or screen border encounter
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private IEnumerator SpawnKillerSquare()
    {
        yield return new WaitForSeconds(1f);
        
        while (true)
        {
            Instantiate(killerSquarePrefab, RandomItemSpawnPos(), transform.rotation);
            
            yield return new WaitForSeconds(Random.Range(id.killerSquareMinSpawnTimeStep, id.killerSquareMaxSpawnTimeStep));
        }
    }
    
    private IEnumerator SpawnPointSquare()
    {
        while (true)
        {
            Instantiate(pointSquarePrefab, RandomItemSpawnPos(), transform.rotation);
            
            yield return new WaitForSeconds(Random.Range(id.pointSquareMinSpawnTimeStep, id.pointSquareMaxSpawnTimeStep));
        }
    }

    private Vector3 RandomItemSpawnPos()
    {
        float y = Random.Range(id.itemMinYSpawnInclusive, id.itemMaxYSpawnExclusive);
        Vector3 itemSpawnPos = Camera.main.ViewportToWorldPoint(new Vector3(id.itemXSpawn, y, id.itemZSpawn));

        return itemSpawnPos;
    }
}
